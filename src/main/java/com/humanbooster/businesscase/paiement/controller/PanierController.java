package com.humanbooster.businesscase.paiement.controller;

import com.humanbooster.businesscase.paiement.services.PaypalService;
import jakarta.servlet.http.HttpServletResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import java.io.IOException;
import java.math.BigDecimal;

@Controller
@RequestMapping("/panier")
public class PanierController {
    @Autowired
    PaypalService paypalService;

    @RequestMapping(value = "", method = RequestMethod.GET)
    public ModelAndView panier(){
        ModelAndView mv = new ModelAndView("panier");
        return  mv;
    }

    @RequestMapping(value = "/payment", method = RequestMethod.GET)
    public void proceed(HttpServletResponse response) throws IOException {
        response.sendRedirect(
                paypalService.createPayment(BigDecimal.valueOf(840.00)).getRedirectUrl()
        );
    }

    @RequestMapping(value = "paypal/capture", method = RequestMethod.GET)
    public ModelAndView capturePayment(@RequestParam("token") String token){
        ModelAndView mv = new ModelAndView("success");
        paypalService.capturePayment(token);

        return mv;
    }

    @RequestMapping(value = "/paypal/success", method = RequestMethod.GET)
    public ModelAndView paymentSuccess(){
        ModelAndView mv = new ModelAndView("success");
        return mv;
    }

    @RequestMapping(value = "/paypal/error", method = RequestMethod.GET)
    public ModelAndView paymentError(){
        ModelAndView mv = new ModelAndView("error");
        return mv;
    }
}
