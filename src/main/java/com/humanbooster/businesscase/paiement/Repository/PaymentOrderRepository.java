package com.humanbooster.businesscase.paiement.Repository;

import com.humanbooster.businesscase.paiement.model.CompletedOrder;
import com.humanbooster.businesscase.paiement.model.PaymentOrder;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PaymentOrderRepository  extends CrudRepository<PaymentOrder, Long> {
}
