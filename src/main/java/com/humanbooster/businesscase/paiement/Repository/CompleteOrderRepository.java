package com.humanbooster.businesscase.paiement.Repository;

import com.humanbooster.businesscase.paiement.model.CompletedOrder;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CompleteOrderRepository extends CrudRepository<CompletedOrder, Long> {
}
