package com.humanbooster.businesscase.paiement.services;

import com.humanbooster.businesscase.paiement.Repository.CompleteOrderRepository;
import com.humanbooster.businesscase.paiement.Repository.PaymentOrderRepository;
import com.humanbooster.businesscase.paiement.model.CompletedOrder;
import com.humanbooster.businesscase.paiement.model.PaymentOrder;
import com.paypal.core.PayPalHttpClient;
import com.paypal.http.HttpResponse;
import com.paypal.orders.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.math.BigDecimal;
import java.util.List;
import java.util.NoSuchElementException;

@Service
public class PaypalService {
    @Autowired
    private PayPalHttpClient payPalHttpClient;
    @Autowired
    CompleteOrderRepository completeOrderRepository;
    @Autowired
    PaymentOrderRepository paymentOrderRepository;
    public PaymentOrder createPayment(BigDecimal fee) {
        OrderRequest orderRequest = new OrderRequest();
        orderRequest.checkoutPaymentIntent("CAPTURE");
        AmountWithBreakdown amountBreakdown = new AmountWithBreakdown().currencyCode("EUR").value(fee.toString());
        PurchaseUnitRequest purchaseUnitRequest = new PurchaseUnitRequest().amountWithBreakdown(amountBreakdown);
        orderRequest.purchaseUnits(List.of(purchaseUnitRequest));
        ApplicationContext applicationContext = new ApplicationContext()
                .returnUrl("http://localhost:8080/panier/paypal/capture")
                .cancelUrl("http://localhost:8080/panier/paypal/error");
        orderRequest.applicationContext(applicationContext);
        OrdersCreateRequest ordersCreateRequest = new OrdersCreateRequest().requestBody(orderRequest);
        try {
            HttpResponse<Order> orderHttpResponse = payPalHttpClient.execute(ordersCreateRequest);
            Order order = orderHttpResponse.result();
            String redirectUrl = order.links().stream()
                    .filter(link -> "approve".equals(link.rel()))
                    .findFirst()
                    .orElseThrow(NoSuchElementException::new)
                    .href();
            PaymentOrder paymentOrder = new PaymentOrder("success",  order.id(), redirectUrl);
            this.paymentOrderRepository.save(paymentOrder);
            return paymentOrder;
        } catch (IOException e) {
            return new PaymentOrder("error");
        }
    }

    public CompletedOrder capturePayment(String token){
        CompletedOrder completeOrder;
        OrdersCaptureRequest ordersCaptureRequest = new OrdersCaptureRequest(token);
        try {
            HttpResponse<Order> httpResponse = payPalHttpClient.execute(ordersCaptureRequest);
            if (httpResponse.result().status() != null) {
                completeOrder  =  new CompletedOrder("success", token);
            } else {
                completeOrder = new CompletedOrder("error");
            }
        } catch (IOException e) {
            completeOrder = new CompletedOrder("error");
        }

        this.completeOrderRepository.save(completeOrder);

        return completeOrder;
    }


}
